# increments build version
version=`cat build_version`   
version=$(($version + 1))

# builds docker image
sudo docker build . -t petclinic:v$version

# retags and pushes it to dockerhub
sudo docker tag petclinic:v$version devopsnicole/petclinic-p4-jsn:v$version
sudo docker push devopsnicole/petclinic-p4-jsn:v$version

echo $version > build_version