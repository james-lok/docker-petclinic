FROM maven:3.6-jdk-11-slim as BUILD
COPY /spring-petclinic /src
WORKDIR /src
RUN mvn install -DskipTests -Dspring-boot.run.profiles=mysql

FROM openjdk:11.0.1-jre-slim-stretch
EXPOSE 8080
WORKDIR /app

COPY --from=BUILD /src/target/*.jar /app.jar
ENTRYPOINT ["java","-jar","../app.jar"]